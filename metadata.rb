name 'cookbook_rails_app'
maintainer 'Victor Perez'
maintainer_email 'victor.mx14@gmail.com'
license 'All Rights Reserved'
description 'Installs/Configures cookbook_rails_app'
long_description 'Installs/Configures cookbook_rails_app'
version '0.1.0'
chef_version '>= 12.1' if respond_to?(:chef_version)

depends 'opsworks_ruby'
depends 'packages', '~> 1.0.0'
# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
# issues_url 'https://github.com/<insert_org_here>/cookbook_rails_app/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
# source_url 'https://github.com/<insert_org_here>/cookbook_rails_app'
