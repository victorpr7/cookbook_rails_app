#
# Cookbook:: cookbook_rails_app
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

node.default['packages-cookbook'] = [
 'nodejs',
 'imagemagick'
]